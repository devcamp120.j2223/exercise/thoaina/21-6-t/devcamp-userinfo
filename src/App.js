import { gUserInfo } from "./info";
import image from "./assets/images/avatardefault_92824.webp"

function App() {
  return (
    <div className="App">
      <h5>{gUserInfo.firstname + " " + gUserInfo.lastname}</h5>
      <img src={image} width="300px" />
      <p>Age: {gUserInfo.age}</p>
      <p>{gUserInfo.age <= 35 ? "Anh ấy còn trẻ" : "Anh ấy đã già"}</p>
      <ul>
        {
          gUserInfo.language.map((value, index) => {
            return <li key={index}>{value}</li>
          })
        }
      </ul>
    </div>
  );
}

export default App;
